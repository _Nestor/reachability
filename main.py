
# initialize Qt resources from file resources.py
import datetime

from qgis._core import QgsMapLayerRegistry

import resource
from ui.ui import Ui
import utils_giswater
import sys
import os
from PyQt4.QtCore import *
from PyQt4.QtGui import QAction, QIcon,QMessageBox
import psycopg2, psycopg2.extras
import qgis
from datetime import datetime, date
import utils_nes

import ConfigParser

class mainClass():
    def __init__(self, iface):
        self.iface = iface

    def initGui(self):
        # create action that will start plugin configuration
        self.action = QAction(QIcon(":/plugins/reachability/icon.png"), "Reachability plugin", self.iface.mainWindow())
        self.action.setObjectName("testAction")
        self.action.setWhatsThis("Configuration for test plugin")
        self.action.setStatusTip("This is status tip")
        QObject.connect(self.action, SIGNAL("triggered()"), self.run)

        # add toolbar button and menu item
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu("&Reachability plugins", self.action)

        # connect to signal renderComplete which is emitted when canvas
        # rendering is done
        QObject.connect(self.iface.mapCanvas(), SIGNAL("renderComplete(QPainter *)"), self.renderTest)


    def unload(self):
        # remove the plugin menu item and icon
        self.iface.removePluginMenu("&Reachability plugins", self.action)
        self.iface.removeToolBarIcon(self.action)

        # disconnect form signal of the canvas
        QObject.disconnect(self.iface.mapCanvas(), SIGNAL("renderComplete(QPainter *)"), self.renderTest)
    def refresh_layers(self):
        for layer in qgis.utils.iface.mapCanvas().layers():
            layer.triggerRepaint()

    def run(self):
        # create and show a configuration dialog or something similar
        # Create the dialog and signals
        self.dlg = Ui()
        # Function setDialog is writen in utils_giwater python file
        utils_giswater.setDialog(self.dlg)
        # Create signals

        #self.btn_accept=self.dlg.findChild()

        self.dlg.btn_accept.pressed.connect(self.btnaccept)
        self.dlg.btn_exit.pressed.connect(self.dlg.close)
        #self.dlg.cmbArcs.currentIndexChanged.connect(self.populateCmbNode())
        #self.dlg.btn_file.pressed.connect(self.select_file)

        self.populateCmbSchemas()
        self.populateCmbArc()
        self.populateCmbNode()
        self.dlg.cmbArcs.currentIndexChanged.connect(self.populateCmbNode)

        self.dlg.exec_()

        self.refresh_layers()


    def renderTest(self, painter):
        # use painter for drawing to map canvas
        print ("Reachability Plugin: renderTest called!")


    def set_database_connection(self):
        ''' Sets database connnection '''
        # Initialize variables
        # Look for connection data in QGIS configuration (if exists)
        config = ConfigParser.ConfigParser()
        self. path =os.path.dirname(os.path.abspath(__file__))
        config.read(self.path + "\\config.cfg")
        self.connection_name = config.get("postgresConfig", "conecation_name")
        connection_settings = QSettings()
        root_conn = "/PostgreSQL/connections/"
        connection_settings.beginGroup(root_conn);
        groups = connection_settings.childGroups();

        if self.connection_name in groups:
            self.root = self.connection_name + "/"
            self.dbHostName = connection_settings.value(self.root + "host", '')
            self.port = connection_settings.value(self.root + "port", '')
            self.dbName = connection_settings.value(self.root + "database", '')
            self.dbUsername = connection_settings.value(self.root + "username", '')
            self.dbPassword = connection_settings.value(self.root + "password", '')
            return True

        else:
            msg = "Database connection name not found. Please check configuration file 'config.cfg'"
            self.show_warning(msg)
            return False

    def conectToDB(self):
        conected = False
        if self.set_database_connection():
            '''
            print ("self.set_database_connection()")
            print(str(self.dbName))
            print(str(self.dbUsername))
            print(str(self.dbPassword))
            print(str(self.dbHostName))
            print(str(self.port))
            '''
            try:
                self.conn = psycopg2.connect(database=self.dbName, user=self.dbUsername, password=self.dbPassword,

                                        host=self.dbHostName)
                conected = True
            except:
                conected = False
            finally:
                return conected
        else:
            #opens an information message box
            request = QMessageBox.warning(None, 'Config error',
                                          'Database connection named ' + self.connection_name + ' not found. \nPlease check configuration file config.cfg',
                                          QMessageBox.Open | QMessageBox.Close, QMessageBox.Close)
            if request == QMessageBox.Open:
                os.system('start " " ' + self.path + '"\\config.cfg"')


    def btnaccept(self):
        if(self.conectToDB()):
            self.cur = self.conn.cursor()
            self.cur.execute("begin")
            distancia = self.dlg.txt_distance.text()
            capaejes=self.dlg.cmbArcs.currentText()
            capanodos = self.dlg.cmbNodes.currentText()
            if( distancia.replace('.','',1).isdigit()):
                print("digit")
                self.cur.execute("UPDATE llicamunt.bus_stop_config SET value=" + distancia
                                 +" WHERE parameter LIKE 'distance'")
                self.cur.execute("UPDATE llicamunt.bus_stop_config SET layers='" + capaejes
                                 +"' WHERE parameter LIKE 'axislayer'")
                self.cur.execute("UPDATE llicamunt.bus_stop_config SET layers='" + capanodos +"' WHERE parameter LIKE 'nodelayer'")
                self.cur.execute("SELECT llicamunt.bus_stop_buffer()")
            else:
                #QMessageBox.about(None, 'Error',  self.dbName)
                QMessageBox.about(None, 'Error', 'The field distance to be numerical')
                print("no digit")
            self.cur.execute("commit")
            for x in QgsMapLayerRegistry.instance().mapLayers().values():
                print(x.name())


    def populateCmbSchemas(self):
        self.dlg.cmbSchemas.clear()
        schemaslist=[]
        if (self.conectToDB()):
            cur = self.conn.cursor()
            cur.execute("begin")
            cur.execute("select schema_name from information_schema.schemata")
            #WHERE schema_name NOT IN ('pg_toast','pg_temp_1','pg_toast_temp_1','pg_catalog','information_schema')
            resultados = cur.fetchall()
            cur.execute("commit")
            for sch in resultados:
                schemaslist.append(sch[0])
            self.dlg.cmbSchemas.addItems(schemaslist)

    def populateCmbArc(self):
        self.dlg.cmbArcs.clear()
        layerlist=[]
        for lyr in QgsMapLayerRegistry.instance().mapLayers().values():
            if lyr.name() != "":
                layerlist.append(lyr.name())
        self.dlg.cmbArcs.addItems(layerlist)

    def populateCmbNode(self):
        self.dlg.cmbNodes.clear()
        layerlist=[]
        for lyr in QgsMapLayerRegistry.instance().mapLayers().values():
            if lyr.name() != self.dlg.cmbArcs.currentText():
                layerlist.append(lyr.name())
        self.dlg.cmbNodes.addItems(layerlist)
        self.populateCmbCost()
    def populateCmbCost(self):
        self.dlg.cmbCost.clear()
        layerlist=[]
        layerfield=[]
        for lyr in QgsMapLayerRegistry.instance().mapLayers().values():
            if lyr.name() == self.dlg.cmbArcs.currentText():
                layerlist.append(lyr.name())
                for field in lyr.pendingFields():
                    #print field.name(), field.typeName()
                    layerfield.append(field.name())
                break
        self.dlg.cmbCost.addItems(layerfield)
        '''
        self.dlg.cmbCost.clear()
        print("layyyeeer")
        print (layerlist)
        layer = qgis.utils.iface.activeLayer()
        #layer = layerlist[0]

        fields = layer.pendingFields()
        field_names = [field.name() for field in fields]
        self.dlg.cmbCost.addItems(field_names)
        '''
        print(" testing populateCmbCost")



    '''
    def savelog(self, name):
        path = os.getcwd()
        print(path)
        #el os.getcwd() nos da como ruta C:\Program Files\QGIS Essen\bin que es una carpeta en la que no podemos escribir
        #    por lo tanto mejor dar ruta absoluta???

        path='C:\\Users\\tasl\Desktop'
        #print(path)
        logfile = open(path + "/log.txt", 'a')
        now = datetime.now()
        now = now.strftime('%d/%m/%y %H:%M:%S')
        logfile.write(now + "\t" + name + "\n")
        logfile.close()
    '''